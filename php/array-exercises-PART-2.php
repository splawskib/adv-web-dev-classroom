<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Array Exercises - Part 2</title>
	<script type="text/javascript">

	// STEP 1
	// Declare a variable named people and initialize it to an empty array
	// var people = new Array();
	var people = [];


	// STEP 2
	/*
	Add an element to the people array.
	This first element should be an object that has the following properties (keys) and values:
	PROPERTY (KEY)		VALUE
	-------------------------------
	id 					"1bs"
	firstName			"BOB"
	lastName 			"SMITH"
	*/
	//people[0] = "foo";
	people.push({id:"1bs", firstName:"BOB", lastName:"SMITH"});



	// STEP 3
	/*
	Add another element to the people array.
	This element should be an object that has the following keys and values:
	PROPERTY (KEY)		VALUE
	-------------------------------
	id 					"2bj"
	firstName			"Betty"
	lastName 			"Jones"
	*/
	people.push({id:"2bj", firstName:"Betty", lastName:"Jones"});




	// STEP 4
	/*
	Use BRACKET NOTATION Add a 'children' property(key) to the first element in the people array.
	The value of this key should be an indexed array that has following strings in it: "Bob Jr." and "Sally"
	*/
	people[0]['children'] = ["Bob Jr.", "Sally"];





	// STEP 5
	/*
	Use DOT NOTATION to add a 'children' property(key) to the second element in the people array.
	The value of this key should be an indexed array that has following strings in it: "Jenny", "Jessie", "Sam"
	*/
	people[1].children = ["Jenny", "Jessie", "Sam"];


	console.log(people);
	console.log(people[1].children[2]);
	console.log(people[1]['children'][2]);

	var input = prompt("Enter the property you wish to see about Betty");
	console.log(people[1][input]);
	</script>
</head>
<body>
<p>
	The script element contains JavaScript problems for you to solve.
	The PHP code in the body contains PHP problems for you to solve.
</p>
<?php

// STEP 1
// declare a variable named $people and initialize it to an empty array
$people = array();


// STEP 2
/*
Add an element to the $people array.
This first element should be an associative array that has the following keys and values:
KEY 				VALUE
-------------------------------
id 					"1bs"
first_name			"BOB"
last_name 			"SMITH"
*/
//array_push($people["id"=>"1lbs", "first_name"=>"BOB", "last_name"=>"SMITH"]);
// $people["id"] = "1lbs";
// $people["first_name"] = "BOB";
// $people["last_name"] = "SMITH";
$people[0] = ["id"=>"1lbs", "first_name"=>"BOB", "last_name"=>"SMITH"];
print_r($people[0]);



// STEP 3
/*
Add another element to the $people array.
This element should be an associative array that has the following keys and values:
KEY 				VALUE
-------------------------------
id 					"2bj"
first_name			"Betty"
last_name 			"Jones"
*/
$people[1] = ["id"=>"2bj", "first_name"=>"Betty", "last_name"=>"Jones"];
print_r($people[1]);



// STEP 4
/*
Add a 'children' key to the first element in the $people array.
The value of this key should be an indexed array that has following strings in it: "Bob Jr." and "Sally"
*/
$people[0] = ["children" => "Bob Jr.", "Sally"];
print_r($people[0]);



// STEP 5
/*
Add a 'children' key to the second element in the $people array.
The value of this key should be an indexed array that has following strings in it: "Jenny", "Jessie", "Sam"
*/
$people[1] = ["children" => "Jenny", "Jessie", "Sam"];
print_r($people[1]);

// STEP 6
/*
Loop through the first element in the $people array and echo out all of it's keys
*/
foreach($people[0] as $d){
	echo("<br>" . $d . "</br>");
}


echo("<br>");
// STEP 7
// Use BRACKET NOTATION and the count() function to echo out the number of children that the first person in array has:

?>
</body>
</html>