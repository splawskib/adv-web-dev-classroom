<?php

$host = "localhost";
$db = "user_test_db";
$user = "root";
$password = "";

$link = mysqli_connect($host, $user, $password, $db);
if(!$link){
    die("Unable to connect");
}else{
    echo("Connection successful!");
    //var_dump($link);
}

$qStr = "SELECT * FROM users";
$result = mysqli_query($link, $qStr);
if(!$result){
    die(mysqli_error($link));
}

//var_dump($result);
/*
if(mysqli_num_rows($result) > 0){
    while($row = mysqli_fetch_assoc($result)){
        var_dump($row);
        echo("<br><br>");
    }
*/

    $users = array();
    if(mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)){
            $u = array();
            $u['user_id'] = htmlentities($row['user_id']);
            $u['user_first_name'] = htmlentities($row['user_first_name']);
            $u['user_last_name'] = htmlentities($row['user_last_name']);
            $u['user_email'] = htmlentities($row['user_email']);
            $u['user_password']= htmlentities($row['user_password']);
            $u['user_salt']= htmlentities($row['user_salt']);
            $u['user_role']= htmlentities($row['user_role']);
            $u['user_active'] = htmlentities($row['user_active']);
            $users[] = $u;
            var_dump($users);
            echo("<br><br>");
        }
        echo("</table>");
    }
    //var_dump($users);
?>