<?php 
$firstName = "Bob";
$today = date("m/d/Y", time()); 

echo("<h1>Hello $firstName</h1>"); 
echo("<p>Today is: $today </p>");
echo($firstName)
echo("blah");
?> 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>First PHP Page</h1>
    <ul>
        <li>PHP code is often mixed into HTML code.</li>
        <li>To use PHP code inside a web page you should save the page witha .php extension</li>
        <li>PHP code goes inside the <b>PHP Delimiters</b>: 
            <pre>
&lt;?php 
// PHP code goes here, in between the opening and closing PHP delimiters. 
?&gt;
            </pre>
        </li>
        <li>
            PHP runs on the web server and it is often used to generate HTML code that is sent to the browser.
            <pre>
&lt;?php 
$firstName = "Bob";
$today = date("m/d/Y", time()); 

echo("&lt;h1&gt;Hello $firstName&lt;/h1&gt;"); 
echo("&lt;p&gt;Today is: $today &lt;/p&gt;");
?&gt;                
            </pre>
        </li>
        <li>Variable names in PHP start with a dollar sign (this is bound to trip you up!)</li>
        <li>Unlike JavaScript, HTML, and CSS, you can't view PHP code in the browser. Because the code runs on the server, you only see the results of the PHP code in the browser.</li>
        <li>
            A typical use case for PHP is this:
            <ul>
                <li>You may use PHP on a page that shows customer orders.</li>
                <li>The user requests the PHP page in the browser</li>
                <li>The request reaches the web server, the web server locates the requested page and executes the PHP code in it before sending the response to the browser.</li>
                <li>The PHP code fetches the customer orders rows from a database</li>
                <li>The PHP code takes the results from the database and generates HTML tags to display a table in the page</li>
                <li>The HTML tags are sent to the browser (the result of the PHP code, but not the PHP code itself)</li>
            </ul>
        </li>
    </ul>
</body>
</html>