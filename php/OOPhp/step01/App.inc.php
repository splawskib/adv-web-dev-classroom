<?php
class User{
    public $firstName;
    public $lastName;

    public function __construct($firstName, $lastName){
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function sayHello(){
        echo("Hello, I'm " . $this->firstName . "<br>");
    }
}