<?php

class App{

    private $users = array();

    function addUser($u){

        $this->users[] = $u;
    }

    function showUsers(){

        echo("<h3>Here are the users in this system</h3>");
        echo("<ol>");

        foreach ($this->users as $u){
            echo("<li>" . $u->firstName . " " . $u->lastName . "</li>");
        }

        echo("</ol>");
    }
}