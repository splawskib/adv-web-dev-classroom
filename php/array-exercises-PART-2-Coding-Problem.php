<?php 
$people = [ 
	['id' => "1", 'first_name' => 'Bob', 'last_name' => "Smith", 'children'  => ["Bob Jr.", "Sally", "sam", "pay"]],
	['id' => "2", 'first_name' => 'Betty', 'last_name' => "Jones", 'children'  => ["Jenny", "Jesse", "Sam"]]
];

/*
Write a function named get_max_children() that loops through the $people array and returns the max number of children that a person has.
Then call the function and echo the return value (it should return 3).
HINT: To get the length of an array in PHP, you must use the count() function
*/
function get_max_children(){
	global $people;
	$person = 0;
	foreach($people as $e){
		if(count($e['children']) > $person){
			$person = count($e['children']);
		}
	}
	return $person;
}


echo(get_max_children());



?>