<!DOCTYPE html>
<html>
<head>
<title>Array Exercises</title>
<script type="text/javascript">
window.addEventListener("load", ()=>{
	
	var employees = [
		{id:1, firstName:"Betty", lastName:"Smith", salary:55000 },
		{id:2, firstName:"Bo", lastName:"Hansen", salary:35000 },
		{id:3, firstName:"Chris", lastName:"Jones", salary:45000 },
		{id:4, firstName:"John", lastName:"Ortega", salary:75000 },
		{id:5, firstName:"Cliff", lastName:"Long", salary:65000 },
		{id:6, firstName:"Charlie", lastName:"Green", salary:60000 },
		{id:7, firstName:"Tom", lastName:"Black", salary:52000 },
		{id:8, firstName:"Sara", lastName:"Gray", salary:80000 },
		{id:9, firstName:"Lisa", lastName:"Johnson", salary:31000 },
		{id:10, firstName:"Michelle", lastName:"Link", salary:55000 }
	];

	// To loop through the properties (keys) in an object use a for in loop
	for(var p in employees[0]){
		console.log(p);
		console.log(employees[0][p]);
	}

	var e = employees[0];
	for (var p in e){
		console.log(p);
		console.log(e[p]);
	}


	/*
	Problem 1
	Write a function named getMaxSalary
	The function should loop through the array and return the highest salary
	After you declare the function, invoke it and console log the return value.   
	*/
	// functon getMaxSalary (){
	// 	var max = 0;
	// 	for(var x = 0; x < employees.length; x++)
	// }



	/*
	Problem 2
	Write a function named getById
	The function should have a single parameter, which should be an id number
	In the body of the function, loop through the employees array in search of the one whose id matches the parameter
	The function should return the employee object whos id matches the parameter
	If there is no employee whos id matches the parameter, then the function should return false

	After you write the function, invoke it and pass in a value of 7
	Then console log the return value
	*/
	function getById(id) {
		for(var x = 0; x < employees.length; x++){
			var y = employees[x].id;

			if (y = id){
				console.log("The employee matches");
			}else{
				console.log("The employee does not match");
			}
			break;
		}
	}

	getById(7);
	


	/*
	Problem 3
	Write a function named getHighestPaidEmployee
	It should return the employee with the highest salary

	After you declare the function, invoke it and console log the return value
	*/
	function getHighestPaidEmployee(){
		var x = 0;

		for(var y = 0; y < employees.length; y++){
			var z = employees[y].salary

			if (z > x){
				x = z;
			}
		}
		console.log(x);
	}

	getHighestPaidEmployee();



	/*
	Problem 4
	Use the splice array method to remove Charlie Green from the employees array
	*/
	var employeeSplice = employees.splice(6, 1);

	console.log(employeeSplice);



	
});
</script>
</head>
<body>
	<h1>Working with Arrays in JavaScript and PHP</h1>
	<p>
		There are 4 JavaScript problems for you to solve (see the srcript element inside the head).
		Then there are 4 PHP problems for you to solve (in the body below).
	</p>
<?php
echo("<h3>PHP Problems</h3>");

$employees = array(
	["id" => 1, "firstName" => "Betty", "lastName" => "Smith", "salary" => 55000],
	["id" => 2, "firstName" => "Bo", "lastName" => "Hansen", "salary" => 35000],
	["id" => 3, "firstName" => "Chris", "lastName" => "Jones", "salary" => 45000],
	["id" => 4, "firstName" => "John", "lastName" => "Ortega", "salary" => 75000],
	["id" => 5, "firstName" => "Cliff", "lastName" => "Long", "salary" => 65000],
	["id" => 6, "firstName" => "Charlie", "lastName" => "Green", "salary" => 60000],
	["id" => 7, "firstName" => "Tom", "lastName" => "Black", "salary" => 52000],
	["id" => 8, "firstName" => "Sara", "lastName" => "Gray", "salary" => 80000],
	["id" => 9, "firstName" => "Lisa", "lastName" => "Johnson", "salary" => 31000],
	["id" => 10, "firstName" => "Michelle", "lastName" => "Link", "salary" => 55000]
);



echo("<h4>Problem 1</h4>");
/*
Problem 1
Write a function named get_max_salary
The function should loop through the array and return the highest salary
After you declare the function, invoke it and echo  the return value.   
*/
function get_max_salary(){
	global $employees;
	$salary = 0;
	for($x = 0; $x < count($employees); $x++){
		$e = $employees[$x];
		if($e['salary'] > $salary){
			$salary = $e['salary'];
		}
	}
	return $salary;
}

print_r(get_max_salary());






echo("<h4>Problem 2</h4>");
/*
Problem 2
Write a function named get_by_id
The function should have a single parameter, which should be an id number
In the body of the function, loop through the employees array in search of the one whose id matches the parameter
The function should return the employee object who's id matches the parameter
If there is no employee whos id matches the parameter, then the function should return false

After you write the function, invoke it and pass in a value of 7
Then var_dump the return value (pass in the return value to the var_dump() function)
*/
function get_by_id($id){
	global $employees;
	for($x = 0; $x < count($employees); $x++){
		$e = $employees[$x];
		if($e['id'] == $id){
			return $e;
		}
	}
	return false;
}

var_dump(get_by_id(7));





echo("<h4>Problem 3</h4>");
/*
Problem 3
Write a function named get_highest_paid_employee
It should return the employee with the highest salary
This function should take an array of employees as a parameter
(some people think that you shouldn't use the global keyword inside the body of a function, instead you should pass in the required information as a parameter)

After you declare the function, invoke it (pass in the $employees array as a param) and  then var_dump the return value
*/
function get_highest_paid_employee($employees){
	$salary = 0;
	$emp = 0;
	for($x = 0; $x < count($employees); $x++){
		$e = $employees[$x];
		if($e['salary'] > $salary){
			$salary = $e['salary'];
			$emp = $e;
		}
	}
	return $emp;
}

var_dump(get_highest_paid_employee($employees));


echo("<h4>Problem 4</h4>");
/*
Problem 4
Use the array_splice function to remove Charlie Green from the employees array.
Hint: Here's the official documentation on the array_splice function in PHP - https://www.php.net/manual/en/function.array-splice.php
Then var_dump the employees array.
*/
array_splice($employees, 5, 1);
var_dump($employees);


?>
</body>
</html>