<?php
/*
// In page-data-access-tests.php here's what gets var dumped when we fetch all pages (including ones that are not 'active')
array (size=2)
  0 => 
    array (size=5)
      'pageId' => string '2' (length=1)
      'path' => string 'another-test.php' (length=16)
      'title' => string 'Another Test Blog Post' (length=22)
      'publishedDate' => string '06/26/2018' (length=10)
      'active' => string 'no' (length=2)
  1 => 
    array (size=5)
      'pageId' => string '1' (length=1)
      'path' => string 'test-blog.php' (length=13)
      'title' => string 'This is a test blog post' (length=24)
      'publishedDate' => string '06/25/2018' (length=10)
      'active' => string 'yes' (length=3)
*/

/*
Notice that is is a multi-dimensional array (an array of arrays)
To be more specific, it's an indexed array of associative arrays (key/value pairs)

You must be able to understand multi-dimensional arrays like this one because
that's what results from SQL queries look like.
You can think of a SQL result as a multi-dimensional array - 
an array of rows, and each row is an array of columns (or cells)

This var dump is similar to the syntax for declaring this array.
The var dump includes extra information about the array, such as the size of each array,
and the data type of each element in the array.
Note that if an element is a string, it displays the length of the string as well.

If we wanted to recreate this same exact data structure in our PHP code, we could do it like this:
*/

$allPages = array(
				array(
			      'pageId' =>'2',
			      'path' =>'another-test.php',
			      'title' =>'Another Test Blog Post',
			      'publishedDate' =>'06/26/2018',
			      'active' =>'no'
			    ),
			    array(
			      'pageId' =>'1',
			      'path' =>'test-blog.php',
			      'title' =>'This is a test blog post',
			      'publishedDate' =>'06/25/2018',
			      'active' =>'yes'
			    )
			);

/*
Notice that this approach uses the array() function in PHP to create each array.
But, in PHP 7 they introduced bracket notation as well. So you could do it this way:
*/

$allPages = [
	[
	  'pageId' =>'2',
	  'path' =>'another-test.php',
	  'title' =>'Another Test Blog Post',
	  'publishedDate' =>'06/26/2018',
	  'active' =>'no'
	],
	[
	  'pageId' =>'1',
	  'path' =>'test-blog.php',
	  'title' =>'This is a test blog post',
	  'publishedDate' =>'06/25/2018',
	  'active' =>'yes'
	]
];

// If you wanted to save some vertical space, you could put each of the 
// associative arrays on one line like so:
//	['pageId' =>'2', 'path' =>'another-test.php', 'title' =>'Another Test Blog Post', 'publishedDate' =>'06/26/2018', 'active' =>'no']

echo("<h3>Step 1</h3>");
// echo the title of the second blog post
echo($allPages[1]['title']);
// Answer:



echo("<h3>Step 2</h3>");
// loop through the $allPages array and figure out how many of the blog posts are 'active'
// Then echo out your answer

// Answer:
$numberOfActivePages = 0;
foreach($allPages as $p){
	// var_dump($p['active']);
	if($p['active']){
		$numberOfActivePages++;
	}
}
echo($numberOfActivePages);

for($x = 0; x < count($allPages); $x++){

}

foreach($allPages[0] as $value){

}

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Arrays Part 3</title>
<script type="text/javascript">
/*
In JavaScript, we work with tabular data as arrays of objects.
Luckily, these are very similar to arrays of assiociative arrays.
They are both key/value pairs, but when it comes to objects we usually refer to the keys as 'properties'.
	(SIDE NOTE: 
		Remember that when you boil them down to the basics, objects are made up of properties and methods.
		'Properties' can be an ambiguous term because the meaning can be nuanced depending on the specific language
		In C# properties are really the equivalent of setters/getters in Java (the protect access to instance variables)
		To make things more confusing, some people use 'properties' and 'instance variables' interchangeably
		Earlier we said that objects are essentially made of properties and methods.
		I suppose you could also say that they are made of instance variables and methods

Here's what our data would look like in JavaScript:
*/

var allPages = [
	{
	  'pageId':'2',
	  'path':'another-test.php',
	  'title':'Another Test Blog Post',
	  'publishedDate':'06/26/2018',
	  'active':'no'
	},
	{
	  'pageId':'1',
	  'path':'test-blog.php',
	  'title':'This is a test blog post',
	  'publishedDate':'06/25/2018',
	  'active':'yes'
	}
];

//Step 1
// Console log the title of the second blog post
// BUT NOTE THAT WE NORMALLY USE DOT NOTATION WHEN WE WORK WITH OBJECTS IN JAVASCRIPT
// Although, you could use bracket notation

// Answer:
console.log(allPages[1].title);
console.log(allPages[1]['title']);


// Step 2;
// loop through the allPages array and figure out how many of the blog posts are 'active'
// Then echo out your answer

// Answer:

	
</script>
</head>
<body>

</body>
</html>







