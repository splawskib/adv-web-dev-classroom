<?php 
$people = [ 
	['id' => "1", 'first_name' => 'Bob', 'last_name' => "Smith", 'children'  => ["Bob Jr.", "Sally","Jenny", "Jesse", "Sam"]],
	['id' => "2", 'first_name' => 'Betty', 'last_name' => "Jones", 'children'  => ["Jenny", "Jesse", "Sam"]]
];

/*
// Here's a good example...
function get_max_children(){
	global $people;
	$cNumberOfChildren;
	$hNumberOfChildren =0;
	foreach($people as $p){
		$cNumberOfChildren = count($p['children']);
		if($cNumberOfChildren > $hNumberOfChildren){
			$hNumberOfChildren = $cNumberOfChildren;
		}
	}
	return $hNumberOfChildren;
}
echo(get_max_children());
*/

/*
// this one took me a minute to figure out why it doesn't work properly
(note that I added a few children to Bob to prove that it didn't work)
function get_max_children(){
	global $people;
	$person = 0;
	foreach($people as $e){
		if($e['children'] > $person){
			$person = count($e['children']);
		}
	}
	return $person;
}

echo(get_max_children());
*/



// This one has some issues, but somehow the boolean expression in the
// IF statement seems to work (which I don't understand)
function get_max_childern(){
	global $people;
	$max = 0;
	foreach($people as $p){
		if($p['children'] > $max){
			$max = $p['children'];
		}
	}

	return $max;
}

echo count(get_max_childern());



/*
// This one has various issues
function get_max_children(){
	global $people;
	$maxChildrens = 0;
	 foreach($people as $p){
		if($p['children'] > $maxChildrens) {
			$maxChildrens = $p['children'];
		}
		
	 }
echo($maxChildrens);
}
//get_max_children();
echo(count($people[1]['children']));
*/




?>