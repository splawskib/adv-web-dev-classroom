<?php
//echo($_SERVER['REQUEST_METHOD']);

// Set up variables for the input and set them to default values
$first_name = "";
$last_name = "";
$order_type = "";
$crust = "";
$meat = array();
$veggies = array();
$terms = "";

$validation_errs = array();

if($_SERVER['REQUEST_METHOD'] == "POST"){
  //var_dump($_POST);
  //print_r($_POST);

  // Initialize variables from the $_POST array
  $first_name = $_POST['first_name'];
  $last_name = $_POST['last_name'];
  $order_type = $_POST['order_type'] ?? $order_type; // NULL COALESCING OPERATOR IS ??
  $crust = $_POST['crust'];
  $meat = $_POST['meat'] ?? $meat;
  $veggies = $_POST['veggies'] ?? $veggies;
  $terms = $_POST['terms'] ?? $terms;

  // Validate the user input
  if(empty($first_name)){
    $validation_errs['first_name'] = "You must enter your first name";
  }

  if(empty($last_name)){
    $validation_errs['last_name'] = "You must enter your last name";
  }

  if(empty($order_type)){
    $validation_errs['order_type'] = "You must choose an order type";
  }

  if($crust == "Choose one..."){
    $validation_errs['crust'] = "Choose a crust";
  }

  if($terms != "agreed"){
    $validation_errs['terms'] = "You must agree to our terms";
  }

  // Check the errs array to see if it's empty, if so then good to go
  if(empty($validation_errs)){
    echo("<h3>Thank you, your pizza is comming right up</h3>");
    die();
  }else{
    // foreach($validation_errs as $msg){
    //   echo($msg . "<br>");
    // }

      foreach($validation_errs as $key => $value){
        $validation_errs[$key] = "<span class='validation'>$value</span>";
      }
  }

}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <title>Posting Forms</title>
    <style>
      .validation{ color: red; }
    </style>
  </head>
  <body>
  <h1>Pizza Order Form</h1>
  <form method="POST" action="<?php echo($_SERVER['PHP_SELF']); ?>" >
      
      <label>First Name:</label>
      <br> 
      <input type="text" name="first_name" value="<?php echo($first_name); ?>" />
      <?php echo($validation_errs['first_name'] ?? ""); ?>
      <br><br>
      
      <label>Last Name:</label>
      <br>
      <input type="text" name="last_name" value="<?php echo($last_name); ?>" />
      <?php echo($validation_errs['last_name'] ?? ""); ?>
      <br><br>
      
      <label>Order Type:</label>
      <br>
      <label>
        <input type="radio" name="order_type" value="carry out" <?php echo($order_type == "carry out" ? "checked" : ""); ?> /> Carry Out
      </label>
      <label>
        <input type="radio" name="order_type" value="delivery" <?php echo($order_type == "delivery" ? "checked" : ""); ?> /> Delivery
      </label>
      <?php echo($validation_errs['order_type'] ?? ""); ?>
      <br><br>
      
      <label>Crust:</label>
      <br>
      <select name="crust">
        <option>Choose one...</option>
        <option <?php echo($crust == "Thin" ? "selected" : ""); ?>>Thin</option>
        <option <?php echo($crust == "Pan" ? "selected" : ""); ?>>Pan</option>
        <option <?php echo($crust == "Deep Dish" ? "selected" : ""); ?>>Deep Dish</option>
      </select>
      <?php echo($validation_errs['crust'] ?? ""); ?>
      <br><br>
      
      <label>Meats:</label>
      <br>
      <label>
        <input type="checkbox" name="meat[]" value="sausage" <?php echo(in_array("sausage", $meat) ? "checked" : ""); ?> /> Sausage
      </label>
      <label>
        <input type="checkbox" name="meat[]" value="pepperoni" <?php echo(in_array("pepperoni", $meat) ? "checked" : ""); ?> /> Pepperoni
      </label>
      <label>
        <input type="checkbox" name="meat[]" value="mushrooms" <?php echo(in_array("mushrooms", $meat) ? "checked" : ""); ?> /> Mushrooms
      </label>
      <br><br>

      <label>Veggies:</label>
      <br>
      <select multiple name="veggies[]">
        <option value="1" <?php echo(in_array(1, $veggies) ? "selected" : ""); ?> >Onions</option>
        <option value="2" <?php echo(in_array(2, $veggies) ? "selected" : ""); ?>>Mushrooms</option>
        <option value="3" <?php echo(in_array(3, $veggies) ? "selected" : ""); ?>>Green Peppers</option>
      </select>
      <br><br>

      <label>
        <input type="checkbox" name="terms" value="agreed" <?php echo($terms == "agreed" ? "checked" : ""); ?> /> Agree to our terms of service
        <?php echo($validation_errs['terms'] ?? ""); ?>
      </label>
      <br><br>

      <input type="submit" value="Submit" />
  </form>

  </body>
</html>
