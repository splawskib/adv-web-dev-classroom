<!docutype html>
<html lang="en">
<head>
    <meta charset= "utf-8">
    <title>Comments</title>
</head>
<body>
<?php

#script 1.4 - comments.php

echo '<p>This is a line of text.<br>This is another line of text.</p>';

/*
echo 'This line will not be executed.';
*/

echo "<p>Now I'm done.</p>"; //end of PHP code

?>
</body>
</html>