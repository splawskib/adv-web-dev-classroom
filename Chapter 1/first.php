<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Basic PHP Page</title>
    </head>
    <body>
        <!-- Script 1.2 - first.php-->     
        <p>This is standard HTML.</p>
        <!-- Script 1.3 - first php-->
    <?php
    // Script 1.3 - first php
    echo 'This was generated using PHP!';
    ?>
    </body>
</html>