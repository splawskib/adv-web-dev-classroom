window.addEventListener("load", function(){
	
	// Here is the data that we'll be working with:
	var users = [
		{id:1, firstName:"Jane", lastName:"Doe", email:"jdoe@acme.com", password:"foo", birthdate:"02/03/2001"},
		{id:2, firstName:"Tony", lastName:"Thompsom", email:"tony@acme.com", password:"bar", birthdate:"7/3/2000"},
		{id:3, firstName:"Jesse", lastName:"Jones", email:"jesse@acme.com", password:"bang", birthdate:"2/3/2002"}
	];


	///////////////////////////////////////////
	// PART 1 - Data binding users to a list
	///////////////////////////////////////////
	var list = document.querySelector("#lsUserList");
	var form = document.getElementById("frmUserDetails");
	var txtId = form.querySelector("#txtId");
	var txtFirstName = form.querySelector("#txtFirstName");
	var txtLastName = form.querySelector("#txtLastName");
	var txtEmail = form.querySelector("#txtEmail");
	var txtPassword = form.querySelector("#txtPassword");
	var txtBirthDate = form.querySelector("#txtBirthDate");
	var btnAddUser = document.querySelector("#btnAddUser");
	var btnDelete = form.querySelector("#btnDelete");

	//Validation controls
	var vFirstName = form.querySelector("#vFirstName");
	var vLastName = form.querySelector("#vLastName");
	var vEmail = form.querySelector("#vEmail");
	var vPassword = form.querySelector("#vPassword");
	var vBirthDate = form.querySelector("#vBirthDate");

	form.addEventListener("submit", function(evt){

		evt.preventDefault();

		if(validate()){
			
			if(txtId.value > 0){
				var user = getUserById(txtId.value);
				updateUser(user);
				displayUserList(users);
			}else{
				var newUser = {};
				updateUser(newUser);
				newUser.id = getMaxId(users) + 1;
				users.push(newUser);
				displayUserList(users);
			}

		}
	});
	
	function displayUserList(userArray){

		list.innerHTML = "";

		for(var x=0; x < userArray.length; x++){
			var li = document.createElement("li");
			li.innerHTML = userArray[x].firstName;
			list.appendChild(li);

		//Add this line in Part 2 (step 2)...
		li.setAttribute("userId", userArray[x].id);
		}
	}

	displayUserList(users);

	function displayUser(u){
		//console.log(u);
		txtId.value = u.id;
		txtFirstName.value = u.firstName;
		txtLastName.value = u.lastName;
		txtEmail.value = u.email;
		txtPassword.value = u.password;
		txtBirthDate.value = u.birthdate;
		btnDelete.style.display = "inline";
	}

	displayUser(users[0]);



	///////////////////////////////////////////
	// PART 2
	// Add validation code
	///////////////////////////////////////////
	function validate(){
		clearValidationMessages();
		var isValid = true;

		if(txtFirstName.value == ""){
			isValid = false;
			vFirstName.innerHTML = "You must enter a first name";
		}

		if(txtLastName.value == ""){
			isValid = false;
			vLastName.innerHTML = "You must enter a last name";
		}

		if(txtEmail.value == ""){
			isValid = false;
			vEmail.innerHTML = "You must enter an email address";
		}else if(validateEmail(txtEmail.value) == false){
			isValid = false;
			vEmail.innerHTML = "You did not enter a valid email address";
		}

		if(txtBirthDate.value == ""){
			isValid = false;
			vBirthDate.innerHTML = "You must enter a date";
		}else if(validateDate(txtBirthDate.value) == false){
			isValid = false;
			vBirthDate.innerHTML = "You did not enter a valid date";
		}

		if(txtPassword.value == ""){
			isValid = false;
			vPassword.innerHTML = "You must enter a password";
		}else if(validatePassword(txtPassword.value) == false){
			isValid = false;
			vPassword.innerHTML = "You did not enter a strong enough password";
		}

		function validateEmail(str){
			var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			return regex.test(str);
		}

		function validateDate(str){
    		var validationD = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    		return validationD.test(str);
		}

		function validatePassword(str){
			var validationP = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*()]).{8,}/;
			return validationP.test(str);
		}




		return isValid;
	}

	function clearValidationMessages(){
		form.querySelectorAll(".validation").forEach((span) => span.innerHTML = "")
	}
	

	
	///////////////////////////////////////////
	// PART 3 - Editing Users
	///////////////////////////////////////////

	function getUserById(userId){
		for(var x=0; x < users.length; x++){
			if(userId == users[x].id){
				return users[x];
			}
		}
	}

	//console.log(getUserById(2));

	list.addEventListener("click", function(evt){

		var userId = evt.target.getAttribute("userId");
		var selectedUser = getUserById(userId);
		if(selectedUser){
			displayUser(selectedUser);
		}
	});

	//Step 3 - bind the data from the form to a user object
	function updateUser(user){

		user.firstName = txtFirstName.value;
		user.lastName = txtLastName.value;
		user.email = txtEmail.value;
		user.password = txtPassword.value;
		user.birthdate = txtBirthDate.value;

		clearForm();
	}

	function clearForm(){
		txtId.value = "";
		txtFirstName.value = "";
		txtLastName.value = "";
		txtEmail.value = "";
		txtPassword.value = "";
		txtBirthDate.value = "";
		btnDelete.style.display = "none";
	}

	///////////////////////////////////////////
	// PART 4 - Adding Users
	///////////////////////////////////////////

	btnAddUser.addEventListener("click", function(){
		clearForm();
		txtFirstName.focus();
	});

	function getMaxId(userArray){
		var maxId = 0;
		for(var x = 0; x < userArray.length; x++){
			if(maxId < userArray[x].id){
				maxId = userArray[x].id;
			}
		}
		return maxId;
	}
	
		

	///////////////////////////////////////////
	// PART 5 - Deleting Users
	///////////////////////////////////////////

	btnDelete.addEventListener("click", function(){
		var id = txtId.value;
		var user = getUserById(id);
		if(confirm(`Are you sure you want delete ${user.firstName} ${user.lastName}?`)){
			var indexOfUser = users.indexOf(user);
			users.splice(indexOfUser,1);
			displayUserList(users);
			clearForm();
		}
	});
	


});